﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileReader.Entities
{
    public class Customer
    {
        public string CPF { get; set; }
        public string Name { get; set; }
        public string Salary { get; set; }
    }
}
