﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileReader.Entities
{
    public class Item
    {
        public int ID { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
